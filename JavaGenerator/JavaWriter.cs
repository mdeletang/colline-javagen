﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaGenerator
{
    public static class JavaWriter
    {
        private static string _path;
        private static List<MethodObject> _methods;
        private static RessourceClassAndroidHttp _ressources;

        public static void Init(string path, List<MethodObject> methods)
        {
            _path = path;
            _methods = new List<MethodObject>();
            _ressources = new RessourceClassAndroidHttp();
            foreach (MethodObject elem in methods)
            {
                _methods.Add(elem);
            }
        }

        private static void WriteImport()
        {
            using (StreamWriter writer = new StreamWriter(_path))
            {
                writer.WriteLine(_ressources.GetRessource(RessourceClassAndroidHttp.IMPORT));
            }
        }

        private static void WriteClass()
        {
            using (StreamWriter writer = new StreamWriter(_path, true))
            {
                writer.WriteLine(_ressources.GetRessource(RessourceClassAndroidHttp.CLASS));
            }
        }

        private static void WritePrivates()
        {
            using (StreamWriter writer = new StreamWriter(_path, true))
            {
                writer.WriteLine(_ressources.GetRessource(RessourceClassAndroidHttp.PRIVATE));
            }
        }

        private static void WriteConstructor()
        {
            using (StreamWriter writer = new StreamWriter(_path, true))
            {
                writer.WriteLine(_ressources.GetRessource(RessourceClassAndroidHttp.CONSTRUCTOR));
            }
        }

        private static void CloseClass()
        {
            using (StreamWriter writer = new StreamWriter(_path, true))
            {
                writer.WriteLine("}");
            }
        }


        private static string GenHeaderMethod(MethodObject method)
        {
            string ret = string.Empty;
            string name = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(method.Controller.ToLower()) + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(method.Action.ToLower());

            ret = string.Format(_ressources.GetRessource(RessourceClassAndroidHttp.HEADERMETHOD), name) + "(";
            if (method.Params.Count == 0)
            {
                ret += "final IAsyncOnUI callback) {";
            }
            for (int i = 0; i != method.Params.Count; ++i)
            {
                if (i == method.Params.Count - 1)
                {
                    ret += "final String " + method.Params[i] + ", final IAsyncOnUI callback) {";
                }
                else
                {
                    ret += "final String " + method.Params[i] + ", ";
                }
            }
            return ret;
        }

        private static string GenContentMethod(MethodObject method)
        {
            string ret = string.Empty;
            string content = string.Empty;

            ret = _ressources.GetRessource(RessourceClassAndroidHttp.THREAD);
            content = "Http" + method.Type + " " + method.Type.ToLower() + " = new Http" + method.Type + @"(""" + _ressources.GetRessource(RessourceClassAndroidHttp.ADDRESS) + method.URL + @""");" + "\n";
            if (method.Params.Count != 0)
            {
                content += "List<NameValuePair> pair = new ArrayList<NameValuePair>();\n";
                foreach (string elem in method.Params)
                {
                    content += @"pair.add(new BasicNameValuePair(""" + elem + @""", " + elem + "));\n";
                }
                content += @"try {
"+ method.Type.ToLower() + @".setEntity(new UrlEncodedFormEntity(pair));
HttpResponse response = mclient.execute(" + method.Type.ToLower() + @");
BufferedReader reader = new BufferedReader(
new InputStreamReader(response.getEntity().getContent()));
String s = reader.readLine();
callback.run(s);
} catch (Exception ex) {
callback.run(""error"");
}
}";
            }
            else
            {
                content += @"try {
HttpResponse response = mclient.execute(" + method.Type.ToLower() + @");
BufferedReader reader = new BufferedReader(
new InputStreamReader(response.getEntity().getContent()));
String s = reader.readLine();
callback.run(s);
} catch (Exception ex) {
callback.run(""error"");
}
}";
            }
            ret = ret.Replace("CONTENT", content);
            return ret;
        }

        private static void WriteMethod(MethodObject method)
        {
            string header;
            string content;

            header = GenHeaderMethod(method);
            content = GenContentMethod(method);
            using (StreamWriter writer = new StreamWriter(_path, true))
            {
                writer.WriteLine(header);
                writer.WriteLine(content);
                writer.WriteLine("}");
            }
        }





        private static void WriteMethods()
        {
            foreach (MethodObject elem in _methods)
            {
                WriteMethod(elem);
            }
        }

        public static bool GenFile()
        {
            WriteImport();
            WriteClass();
            WritePrivates();
            WriteConstructor();
            WriteMethods();
            CloseClass();
            return true;
        }
    }
}