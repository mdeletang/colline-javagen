﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaGenerator
{
    public class MethodObject
    {
        public List<string> Params { get; set; }
        public string Type { get; set; }
        public string File { get; set; }
        public string URL { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public MethodObject()
        {
            Params = new List<string>();
        }

        public override string ToString()
        {
            string ret = string.Empty;

            ret += "Type: " + Type + "\tUrl: " + URL + Environment.NewLine;
            if (File != null)
                ret += "File: " + File + Environment.NewLine;
            ret += "Controller: " + Controller + "\tAction: " + Action + Environment.NewLine;
            foreach (string elem in Params)
            {
                ret += "Argument: " + elem + Environment.NewLine;
            }
            return ret;
        }
    }
}