﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.Diagnostics;

namespace JavaGenerator
{
    public static class MethodGenerator
    {
        public static List<MethodObject> Methods;
        private static List<string> _HTTPTypes;

        public static void Init()
        {
            Methods = new List<MethodObject>();
            _HTTPTypes = new List<string>();
            _HTTPTypes.Add("Get");
            _HTTPTypes.Add("Post");
            _HTTPTypes.Add("Put");
            _HTTPTypes.Add("Delete");
        }


        #region Recuperation

        private static string GetURL(string line)
        {
            string ret;

            ret = line.Substring(line.IndexOf('\'') + 1, line.LastIndexOf('\'') - line.IndexOf('\'') - 1);
            foreach (string elem in _HTTPTypes)
                ret = ret.Replace(elem.ToLower(), "");
            ret = ret.Replace(" ", "");
            ret = ret.Replace("\t", "");
            return ret;
        }

        private static string GetHTTPType(string line)
        {
            string typeAndUrl;

            typeAndUrl = line.Substring(line.IndexOf('\'') + 1, line.LastIndexOf('\'') - line.IndexOf('\''));
            foreach (string elem in _HTTPTypes)
            {
                if (line.Contains(elem.ToLower()))
                    return elem;
            }
            return "Get";
        }

        private static string GetController(string line)
        {
            string ret;

            ret = line.Split(('\''))[1];
            return ret;
        }

        private static string GetAction(string line)
        {
            string ret;

            ret = line.Split(('\''))[1];
            return ret;
        }

        #endregion

        #region Parsing

        private static bool IsCommentedLine(string line)
        {
            Regex reg = new Regex("(^([ ]|[\t])*(//)+)|(^([ ]|[\t])*[*])|(^([ ]|[\t])*[/*])");

            return reg.IsMatch(line);
        }

        private static bool IsRouteLine(string line)
        {
            Regex reg = new Regex("[']{1}.+[']{1}([ ]|[\t])*[:]{1}([ ]|[\t])*[{]{1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        private static bool IsControllerLine(string line)
        {
            Regex reg = new Regex("(controller){1}([ ]|[\t])*[:]{1}([ ]|[\t])*[']{1}.*[']{1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        private static bool IsActionLine(string line)
        {
            Regex reg = new Regex("(action){1}([ ]|[\t])*[:]{1}([ ]|[\t])*[']{1}.*[']{1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        #endregion

        private static void CreateMethod(StreamReader reader, string line)
        {
            MethodObject method = new MethodObject();

            method.URL = GetURL(line);
            method.Type = GetHTTPType(line);
            while ((line = reader.ReadLine()) != null && !IsRouteLine(line))
            {
                if (IsControllerLine(line))
                    method.Controller = GetController(line);
                if (IsActionLine(line))
                    method.Action = GetAction(line);
            }
            if (line != null && IsRouteLine(line))
                CreateMethod(reader, line);
            Methods.Add(method);
        }

        public static void ParseRouteFile(string pathToRoute)
        {
            try
            {
                using (StreamReader reader = new StreamReader(pathToRoute))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        if (IsRouteLine(line))
                            CreateMethod(reader, line);
                    }
                }
                foreach (MethodObject elem in Methods)
                {
                    Debug.WriteLine(elem.ToString());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }






        #region Recuperation

        private static string GetArgument(string line)
        {
            string ret = "";
            string[] tab = line.Split('\'', '"');

            if (tab.Length < 2)
                Debug.WriteLine("GETARGNAME line: " + line);
            else
                ret = tab[1];
            return ret;
        }

        private static void GetArgs(StreamReader reader, MethodObject method)
        {
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                if (IsArgLine(line))
                    method.Params.Add(GetArgument(line));
                if (IsGenProtoMethodLine(line))
                    return;
            }
        }

        private static bool GetProtoAndArgs(MethodObject method)
        {
            using (StreamReader reader = new StreamReader(method.File))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    if (IsProtoMethodLine(line, method))
                    {
                        // Debug.WriteLine(line);
                        GetArgs(reader, method);
                        return true;
                    }
                }
            }
            return false;
        }

        #endregion

        #region Parsing

        private static bool IsProtoMethodLine(string line, MethodObject method)
        {
            Regex reg = new Regex("(" + method.Action + ":" + ")|(" + method.Action + " :" + ")" + @"(( )+|(\t)+)" + "(function){1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        private static bool IsGenProtoMethodLine(string line)
        {
            Regex reg = new Regex("(function){1}.+(req){1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        private static bool IsArgLine(string line)
        {
            Regex reg = new Regex("(req.param){1}");

            if (IsCommentedLine(line))
                return false;
            return reg.IsMatch(line);
        }

        private static bool FindFile(string path, MethodObject method)
        {
            string[] paths = Directory.GetFiles(path);

            foreach (string elem in paths)
            {
                int i;
                string s;

                i = elem.LastIndexOf(@"\");
                s = elem.Substring(elem.LastIndexOf(@"\"));
                if (elem.Substring(elem.LastIndexOf(@"\")).ToLower().Contains(method.Controller))
                {
                    method.File = elem;
                    Debug.WriteLine("Controller: " + method.Controller + " File: " + elem);
                    return true;
                }
            }
            return false;
        }

        #endregion

        public static bool BuildMethods(string path)
        {
            foreach (MethodObject elem in Methods)
            {
                if (!FindFile(path, elem))
                    Debug.WriteLine("CAN'T FIND FILE");
            }
            foreach (MethodObject elem in Methods)
            {
                GetProtoAndArgs(elem);
            }
            return true;
        }
    }
}