﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JavaGenerator
{
    public class RessourceClassAndroidHttp
    {
        #region

        public static readonly string IMPORT = "IMPORT";
        public static readonly string PRIVATE = "PRIVATE";
        public static readonly string CLASS = "CLASS";
        public static readonly string ADDRESS = "ADDRESS";
        public static readonly string CONSTRUCTOR = "CONSTRUCTOR";
        public static readonly string HEADERMETHOD = "HEADERMETHOD";
        public static readonly string THREAD = "THREAD";

        #endregion

        private string _className;
        private Dictionary<string, string> _ressources = new Dictionary<string, string>();

        public RessourceClassAndroidHttp()
        {
            _className = "HttpHandlerGen";
            _ressources.Add(PRIVATE, @"private static final CookieStore mstore = new BasicCookieStore();
private static final DefaultHttpClient mclient = new DefaultHttpClient();
private static String mId;");
            _ressources.Add(CLASS, "public class " + _className + "\n{");
            _ressources.Add(ADDRESS, "http://api.collineboisee.com");
            _ressources.Add(CONSTRUCTOR, "public " + _className + @"() {
mclient.setCookieStore(mstore);
}");
            _ressources.Add(IMPORT, @"package com.example.http_test1;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;");
            _ressources.Add(HEADERMETHOD, "public void {0}");
            _ressources.Add(THREAD, @"new Thread(new Runnable() {

@Override
public void run() {
CONTENT
}).start();
");
        }

        public string GetRessource(string key)
        {
            string ret;

            _ressources.TryGetValue(key, out ret);
            return ret;
        }
    }
}