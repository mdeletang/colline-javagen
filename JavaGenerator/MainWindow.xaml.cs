﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JavaGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _path = @"C:\Users\marcd_000\Desktop\colline-api\api\controllers";
        private string _pathRoutes = @"C:\Users\marcd_000\Desktop\colline-api\config\routes.js";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButonGogo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            MethodGenerator.Init();
            MethodGenerator.ParseRouteFile(dialog.FileName);
        }

        private void ButtonLink_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.ShowDialog();
            _path = dialog.SelectedPath;
        }

        private void ButtonFinalGogo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MethodGenerator.Init();
            MethodGenerator.ParseRouteFile(_pathRoutes);
            MethodGenerator.BuildMethods(_path);
            foreach (MethodObject elem in MethodGenerator.Methods)
            {
                Debug.WriteLine(elem.ToString());
            }
            //List<MethodObject> methods = MethodGenerator.Methods;
            //JavaWriter.Init(@"C:\Users\marcd_000\Desktop\test.java", methods);
            //JavaWriter.GenFile();
        }
    }
}
